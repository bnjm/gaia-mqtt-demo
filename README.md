# gaia-mqtt-demo

## Introduction

This project demonstrates how to connect to the GAIA ecosystem to via MQTT. Only the very basic connection steps
are demonstrated and there is no rendering or message sequence guarantees (gaia -> client) implemented.

> For a more sophisticated example please refer to [gaia-js-sdk-convey](https://github.com/leftshiftone/gaia-js-sdk-convey)

## Requirements

* node 10+
* yarn or npm

## Run

1. `$ yarn install` || `$ npm install`
2. `$ yarn run`


## Messages

### Inbound

Inbound messages (received from GAIA)

```js

{"elements":[
    {"text":"Bonjour, meine Name ist Lumiere, Ihr persönlicher Wein Sommelier. \nWenn Sie möchten kann ich Ihnen einen passenden Wein zu Ihrem Menü empfehlen oder Ihren Weintyp bestimmen.\n","type":"text"}
 ],"index":0,"type":"container","timestamp":1549892547}

```

The `elements` array contains all renderable elements where the `type` determines the renderable type of the element
(such as `text`, `button` et cetera.). 

Renderables of `type` `container` may contain a set of renderables. The inital element is always a container element.

API Clients may decide for themselves how to render the result. 
Examples can be found in the [gaia-js-sdk-convey](https://github.com/leftshiftone/gaia-js-sdk-convey) project.
Furthermore it is possible that multiple messages may be sent to the client (see example below).

### Outbound

Outbound messages must contain a `body` and a `header`.

`body.type`is the type of the outbound message. Currently the `type` may be `text` for textual input (such as direct input from a html text area) and
`button` to submit a button response.

```js

{
    "body": {
        "type":"text","text":"wein empfehlen\n"
        },
    "header": {
        "identityId":"1337","clientId":"1557391f-82b8-08b1-b35d-e2a99c937fb8","userId":"37075573-d0e4-6c94-b391-aefbb1a16f4a"
    }
}

```

## Example Conversation

**1 - Inbound (initial)**
```js
{
    "elements":[
        {"text":"Bonjour, meine Name ist Lumiere, Ihr persönlicher Wein Sommelier. \nWenn Sie möchten kann ich Ihnen einen passenden Wein zu Ihrem Menü empfehlen oder Ihren Weintyp bestimmen.\n","type":"text"}
    ],"index":0,"type":"container","timestamp":1549892834}
```

**2- Inbound (initial)**

```js
{
    "elements":[
        {"text":"Alors, wie kann ich Ihnen dienen?","type":"text"},
        {"name":"result","text":"Wein empfehlen","type":"button","value":"eyJwYXlsb2FkIjoicmVjb21tZW5kX3dpbmUifQ=="},
        {"name":"result","text":"Weintyp bestimmen","type":"button","value":"eyJwYXlsb2FkIjoiZGV0ZXJtaW5lX3dpbmV0eXBlIn0="}
    ],"qualifier":"prompt:functionalityChoice","index":0,"type":"container","timestamp":1549892834}
```

**3 - Outbound (please suggest me a wine)**

```js

{
    "body": {"type":"text","text":"wein empfehlen\n"},
    "header": {"identityId":"1337","clientId":"e964dec3-63d6-cbee-18f9-8bd2175e8e6e","userId":"51fbc496-2247-26f5-e890-072ece00718e"}
}

```

![visualization #1](docs/first.png)


**4- Inbound (what kind of food will you have for dinner?)**

```js

{
    "elements":[
        {"text":"Welche Art der Speise planen Sie zu kredenzen?","type":"text"},
        {"name":"result","text":"Rind, Schwein, Lamm und Co.","type":"button","value":"eyJwYXlsb2FkIjoibWVhdCJ9"},
        {"name":"result","text":"Geflügel","type":"button","value":"eyJwYXlsb2FkIjoicG91bHRyeSJ9"},
        {"name":"result","text":"Fisch","type":"button","value":"eyJwYXlsb2FkIjoiZmlzaCJ9"},
        {"name":"result","text":"Meeresfrüchte und Schalentiere","type":"button","value":"eyJwYXlsb2FkIjoiZmlzaF9zZWFmb29kIn0="},
        {"name":"result","text":"Vegetarisches","type":"button","value":"eyJwYXlsb2FkIjoidmVnIn0="},
        {"name":"result","text":"Käse","type":"button","value":"eyJwYXlsb2FkIjoiY2hlZXNlIn0="},
        {"name":"result","text":"Dessert","type":"button","value":"eyJwYXlsb2FkIjoiZGVzc2VydCJ9"},
        {"name":"result","text":"Weiteres","type":"button","value":"eyJwYXlsb2FkIjoib3RoZXJzIn0="}
     ],"qualifier":"prompt:askMainIngredientNew","index":0,"type":"container","timestamp":1549894264}

```

**5 - Outbound (fish)**


```js

{
    "body": {"type":"text","text":"fisch"},
    "header": {"identityId":"1337","clientId":"e964dec3-63d6-cbee-18f9-8bd2175e8e6e","userId":"51fbc496-2247-26f5-e890-072ece00718e"}
}
```


**6 - Inbound**

```js

{
    "elements":[
        {"text":"Alors poisson soll es sein! Eine vorzügliche Wahl! Allerdings benötige ich noch die Art des Fisches und die Zubereitungsart: ","type":"text"},
        {"name":"ingredient","text":"Gebraten oder gegrillt","type":"button","value":"eyJwYXlsb2FkIjoiZmlzaF9yb2FzdGVkX2JicSJ9"},
        {"name":"ingredient","text{"elements":[{"text":"*clear*","type":"text"}],"index":0,"type":"container","timestamp":1549892835}":"Geräuchert","type":"button","value":"eyJwYXlsb2FkIjoiZmlzaF9zbW9rZWQifQ=="},
        {"name":"ingredient","text":"Gedämpft oder pochiert","type":"button","value":"eyJwYXlsb2FkIjoiZmlzaF9zdGVhbWVkIn0="}
     ],"qualifier":"prompt:askFishType","index":0,"type":"container","timestamp":1549894399}

```

![visualization #2](docs/second.png)