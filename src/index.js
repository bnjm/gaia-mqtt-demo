// import * as mqtt from 'mqtt';
// import * as uuidv4 from 'uuid/v4'

const mqtt = require('mqtt');
const uuidv4 = require('uuid/v4');

class ExampleGaiaMQTTClient {

    /**
     * Initializes the connection
     */
    constructor() {
        this.identityId = "1337"; // id of the lumiere wine recommendation bot
        this.clientId = uuidv4(); // must be a valid v4 uuid
        this.userId = uuidv4(); // must be a valid v4 uuid
        this.inboundTopic = `GAIA/RAIN/${this.clientId}/${this.identityId}/text/in`; // inboundTopic for text is /GAIA/RAIN/[CLIENT_ID]/[IDENTITY_ID]/text/in
        this.outboundTopic = `GAIA/RAIN/${this.clientId}/${this.identityId}/text/out`; // outboundTopic for text

        this.outboundMessages = [];
    }

    /**
     * After the successful connection a subscription to the inbound topic is required to receive message from the identity.
     * Furthermore a special 'reception' message must be send to the outbound topic to make GAIA aware of our presence and
     * that we are ready to receive messages.
     */
    connect() {
        return new Promise((resolve, reject) => {
            this.client = mqtt.connect("wss://beta.gaia.leftshift.one/mqtt", {
                clientId: this.clientId,
                clean: false
            });
            this.client.on('connect', () => {
                this.client.subscribe(
                    this.inboundTopic, (err) => {
                        if (err) {
                            reject(err)
                        }
                        console.log("successfully subscribed!");
                        // send initial 'reception' message to inform GAIA that we are ready to receive messages
                        this.client.publish(this.outboundTopic, JSON.stringify({
                            header: this._header(),
                            body: {"type": "reception"}
                        }), () => {
                            resolve(this)
                        });
                    }
                )
            });
            this.client.on('message', this._onMessage.bind(this));
        });
    }

    send(message) {
        this.outboundMessages.push(message)
    }

    /**
     * Callback for received messages on the inbound topic
     * @param topic
     * @param message
     * @private
     */
    _onMessage(topic, message) {
        const msg = JSON.parse(message);
        console.log(`inbound: ${JSON.stringify(msg)}`);
        /*
            {"elements":[
                {"text":"Bonjour, meine Name ist Lumiere, Ihr persönlicher Wein Sommelier. \nWenn Sie möchten kann ich Ihnen einen passenden Wein zu Ihrem Menü empfehlen oder Ihren Weintyp bestimmen.\n","type":"text"}
            ],"index":0,"type":"container","timestamp":1549888692}
         */
        const next = this.outboundMessages.pop();
        if (next !== undefined) {
            this._sendMessage(next)
        }
    }

    /**
     * Publishes a message to the outbound topic
     *
     * - Example message:
     *
     *  { "body":{"type":"text","text":"wein empfehlen\n"},
     *    "header":{"identityId":"1337","clientId":"cc91a010-22e1-ecd2-58de-4b2f01f2e26a","userId":"3e11e275-c802-ab2e-51f6-8e40ad678bc3"}
     *  }
     *
     *
     * @param message
     */
    _sendMessage(message) {
        const outboundMessage = JSON.stringify({
            header: this._header(),
            body: message
        });

        console.log(`outbound: ${outboundMessage}`);
        this.client.publish(this.outboundTopic, outboundMessage);
    }

    /**
     * The header is required for every outbound message
     * @returns the header
     * @private
     */
    _header() {
        return {
            "identityId": this.identityId,
            "clientId": this.clientId,
            "userId": this.userId
        }
    }
}

const client = new ExampleGaiaMQTTClient();
client.connect()
    .then(client => {
        client.send({"type": "text", "text": "wein empfehlen\n"});
    });